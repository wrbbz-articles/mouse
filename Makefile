pdf:
	pandoc \
		article/* preamble.yaml \
		--filter pandoc-crossref \
		--filter pandoc-citeproc \
		--lua-filter filters/scholarly-metadata.lua \
		--lua-filter filters/author-info-blocks.lua \
		--pdf-engine xelatex \
	  	--output article.pdf

docx:
		pandoc \
		article/* preamble.yaml \
		--filter pandoc-crossref \
		--filter pandoc-citeproc \
		--lua-filter filters/scholarly-metadata.lua \
		--lua-filter filters/author-info-blocks.lua \
		--output article.docx

all: pdf docx

.PHONY: all

clean:
	rm -rf *.pdf
	rm -rf *.docx
