# Conclusion

In this article, to automate MWM testing, we considered various methods that were
combined into one tool, in contrast to existing commercial and non-commercial 
tools, open and standalone, and created a unique program for tracing rodents
and processing the obtained data.
With its help, we obtained correct data on the trajectory of the mouse movement
during the procedure for conducting the behavioral test, and this is one of the
most difficult stages of the automated analysis of the movement of an object in
the aquatic environment.
Unlike "dry" behavioral tests, the water maze is a more difficult task for
computer tracing, since the dynamic surface of the water creates many 
unnecessary moving factors, from glare of bright light to the complete 
disappearance of the experimental rodent from the field of view (if the mouse is
completely immersed in water). 
At the moment, our software is at the stage of active development, but it can 
already produce standard parameters that are difficult to calculate manually.
The software we offer is self-sufficient, it does not require any massive
third-party programs for its work, and will be effective for any modification 
of the "Morris water maze".
In the future, after final revision, the created program will become a better
analogue of inaccessible commercial programs.

# Acknowledgment

The program to increase the competitiveness of leading Russian universities 
among leading scientific-educational centers (Project 5-100-2020).

# References
