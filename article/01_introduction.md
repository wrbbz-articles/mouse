# Introduction

The Morris Water Maze has become the gold standard for the study of spatial 
memory and learning processes, is a must-have test for phenotyping mutant and
transgenic mice [@crawley], and is often used as a general analysis of cognitive
function [@brandesis], for example, to test the effects of various disorders of 
the nervous system, such as animal models of stroke [@ischaemia], aging [@aging],
neurodegenerative diseases [@dis; @dis1; @dis2; @dis3; @dis4] or the potential 
impact of new therapeutic agents [@hooge].
The basic idea behind this behavioral test is simple - animals, usually rats or
mice, are placed in a large circular pool of water and need to get out of the
water onto a hidden platform.  
The numerous protocols for this test are so sensitive to changes in normal 
function in various areas of the brain, not just the hippocampus, that they can 
be used as an “indicator” of normal cognitive functioning. 

The basic protocol, which aims to diagnose the spatial reference memory, 
consists of a daily series of tests in which an experimental rodent swims in
opaque water looking for a hidden platform [@11; @12; @13].
In this case, the simplest and most revealing parameter is the platform search
latency time.
The latency is calculated from the period between the beginning of the rodent
movement and the time point when it found the location of the platform [@14].
The second revealing parameter is the percentage of time the rodent spent in one
of the four imaginary quadrants the platform is in [@14].
In this modification of the protocol, the last single test is an obligatory step,
when the platform is completely removed from the pool, and the rodent swims for
a certain amount of time in opaque water, where there is no platform.
To evaluate this step, the parameter of the percentage of time spent by the 
rodent in the quadrant where the platform was being is used, as well as another
parameter - the number of intersections of the rodent's path and the area of 
interest (ROI), highlighted in the place where the platform was previously. 

Another MWM protocol is used to test working memory, for this, a condition of 
delayed matching to place is created [@brandesis; @steele; @nakazawa].
The main difference with based protocol is daily random change of the hidden 
platform location and just two tests per day, thus there is a daily change of
the ROI.
Also, the reverse learning protocol is associated with the constant change of 
the platform position [@genetic].  

Recognition learning procedure was developed to study the process of spatial and
nonspatial recognition [@18].
This configuration use two hidden platforms, one of which is standard "right" 
and the other is unstable floating "wrong".
Sometimes the location of the right platform additionally changes daily.
Thus in this case, there are two areas of interest simultaneously, one of which
may change daily.
Other options for conducting the Morris Water Maze include changes such as 
limiting the trajectory of a swimming animal to minimize navigation requirements
(for example, a circular water maze [@brun]), reducing the number of available
external signals between learning trials [@nakazawa], the use of floating or 
folding platforms and other manipulations.

In addition to the parameters already mentioned, more and more complex metrics
are gaining popularity.
For example, learning index, cumulative search error, Whishaw error, angular
deviation error and experimental animal search strategies. 
The Learning Index (or Gallagher's proximity) [@20] is a mean measure of 
proximity calculated from the distance between the ROI (hidden platform or where
the platform was previously) and the current position of the rodent in the pool.
In the original 1993 paper, the proximity was calculated ten times per second, 
and the average was taken every second.
Summation second mean measures of proximity up gives the cumulative search error
for each trials [@21].
The distance between the rodent's starting point of swimming and the platform 
location is sometimes subtracted from the cumulative search error to normalize
the parameter to different starting locations.
Another interesting and indicative parameter for assessing the behavior of an
animal in the "Morris water maze" is Whishaw's error [@22; @23].
To calculate this parameter, an imaginary corridor with an arbitrary width is
constructed, which is determined by the researcher, the central line of this
corridor is a straight line connecting the initial position of the test animal 
with the location of the platform.
The percentage of time spent by the animal in an imaginary corridor is calculated.
The heading angle error is an effective but rarely used parameter [@24]. 
It is defined as the value of the deviation of the angle between the orientation
of the animal at a certain point of its swimming in the pool and the direct path
of the beginning of the movement of the animal towards the platform. 
Sometimes, instead of the initial position of the animal, the change in the
angle of orientation of the animal in space relative to the platform is compared
between every first and second seconds of the test [@24].
Classification methods of search strategies provide additional insight into 
various types of animal behavior [@strategy].
It is considered more objective to classify not entire trajectories, but
individual segments of it [@framework], which makes it possible to identify 
animals with mixed behavior.

Therefore, the availability of high-quality software capable of measuring 
parameters more complex than the latency time of the search is an urgent problem
of modern neurobiological research. 
